/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.trivial_broadcast.behavior;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.broadcast.abstract_reliable_broadcast.action.logic.AcFinalizeBroadcastOfMessage;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;

/**
 * Message handler that handles TrivialBroadcastEnvironment#TrivialBroadcastMessageType messages.
 *
 * @author Erwan Mahe
 */
public class TrivialBroadcastOnReceiveMessageHandler<T_msg, A extends BroadcastPeerAgent> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        T_msg payload = (T_msg) message.getPayload();
        (new AcFinalizeBroadcastOfMessage<T_msg,A>(context.getEnvironmentName(), (A) context.getOwner(),payload)).execute();
    }

}
