/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.trivial_broadcast.behavior;

import max.model.broadcast.abstract_reliable_broadcast.action.AcBroadcastToOtherPeersAndImmediatelyToOneself;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.broadcast.trivial_broadcast.env.TrivialBroadcastEnvironment;


/**
 * Start of the Trivial Broadcast procedure : the peer that wants to broadcast the message starts
 * by simply broadcasting an "TrivialBroadcastEnvironment.TrivialBroadcastMessageType" message carrying the message payload.
 *
 * @author Erwan Mahe
 */
public class TrivialBroadcastStartHandler<T_msg, A extends BroadcastPeerAgent> implements ReliableBroadcastHandler<T_msg, A> {
    @Override
    public void handle(T_msg message, A owner, String relevantEnvironment) {
        (new AcBroadcastToOtherPeersAndImmediatelyToOneself<>(
                relevantEnvironment,
                owner,
                message,
                TrivialBroadcastEnvironment.TrivialBroadcastMessageType,
                TrivialBroadcastEnvironment.TrivialBroadcastMessageType
        )).execute();
    }

}
