/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.trivial_broadcast.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.env.AbstractBroadcastEnvironment;

/**
 * An environment for reliable broadcasts.
 *
 * @author Erwan Mahe
 */
public class TrivialBroadcastEnvironment<T_msg, A extends BroadcastPeerAgent> extends AbstractBroadcastEnvironment<T_msg,A> {

    public static String TrivialBroadcastMessageType = "TRIVIAL_BROADCAST";
    public TrivialBroadcastEnvironment(String messageDeliveryEnvironment) {
        super(messageDeliveryEnvironment);
    }

    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new TrivialBroadcastPeerContext<T_msg,A>((BroadcastPeerAgent) agent, this);
    }

}
