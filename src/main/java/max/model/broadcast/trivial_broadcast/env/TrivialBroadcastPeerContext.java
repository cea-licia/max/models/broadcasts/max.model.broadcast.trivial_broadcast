/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.trivial_broadcast.env;

import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.broadcast.abstract_reliable_broadcast.env.BroadcastPeerContext;
import max.model.broadcast.trivial_broadcast.behavior.TrivialBroadcastOnReceiveMessageHandler;
import max.model.broadcast.trivial_broadcast.behavior.TrivialBroadcastStartHandler;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;




/**
 * Context for peers in a broadcast environment.
 *
 * @author Erwan Mahe
 */
public class TrivialBroadcastPeerContext<T_msg,A extends BroadcastPeerAgent> extends BroadcastPeerContext<T_msg,A> {

    public TrivialBroadcastPeerContext(BroadcastPeerAgent owner, StochasticP2PEnvironment environment) {
        super(owner, environment);
        this.startHandler = (ReliableBroadcastHandler<T_msg, A>) new TrivialBroadcastStartHandler<T_msg,A>();
        this.setOnReceiveHandlerForMessageType(TrivialBroadcastEnvironment.TrivialBroadcastMessageType,new TrivialBroadcastOnReceiveMessageHandler<T_msg, A>());
    }

}
