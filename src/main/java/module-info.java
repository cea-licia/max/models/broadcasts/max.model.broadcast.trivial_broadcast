open module max.model.broadcast.trivial_broadcast {
    // Exported packages
    exports max.model.broadcast.trivial_broadcast.behavior;
    exports max.model.broadcast.trivial_broadcast.env;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;
    requires max.model.broadcast.abstract_reliable_broadcast;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}