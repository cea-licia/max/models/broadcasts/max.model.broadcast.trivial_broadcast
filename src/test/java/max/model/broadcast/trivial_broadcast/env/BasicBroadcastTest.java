/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.trivial_broadcast.env;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.broadcast.abstract_reliable_broadcast.action.config.AcSetBroadcastDeliveryHandler;
import max.model.broadcast.abstract_reliable_broadcast.action.logic.AcStartBroadcastOfMessage;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.network.stochastic_adversarial_p2p.action.config.AcAddCommunicationDelay;
import max.model.network.delay_distributions_lib.distros.ContinuousUniformDelay;
import max.model.network.stochastic_adversarial_p2p.context.filter.UniformLevelFilter;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * Basic Test in which we simply instantiate 2 peers in an environment.
 *
 * @author Erwan Mahe
 */
public class BasicBroadcastTest {

    @BeforeEach
    public void before(@TempDir Path tempDir) {
        clearParameters();

        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }

    @Test
    public void testBroadcast(TestInfo testInfo) throws Throwable {
        final var tester =
                new ExperimenterAgent() {

                    private TrivialBroadcastEnvironment<Integer, BroadcastPeerAgent> Benv;
                    private StochasticP2PEnvironment P2Penv;
                    private BroadcastPeerAgent peer1;
                    private BroadcastPeerAgent peer2;

                    @Override
                    protected List<MAXAgent> setupScenario() {
                        final List<MAXAgent> agents = new ArrayList<>();

                        P2Penv = new StochasticP2PEnvironment();
                        agents.add(P2Penv);

                        Benv = new TrivialBroadcastEnvironment<Integer, BroadcastPeerAgent>(P2Penv.getName());
                        agents.add(Benv);

                        peer1 = new BroadcastPeerAgent(createPeerPlan(P2Penv.getName(), Benv.getName(),true));
                        agents.add(peer1);

                        peer2 = new BroadcastPeerAgent(createPeerPlan(P2Penv.getName(), Benv.getName(),false));
                        agents.add(peer2);

                        return agents;
                    }
                };
        launchTester(tester, 50, testInfo);
    }

    private Plan<BroadcastPeerAgent> createPeerPlan(String P2PenvName, String BenvName, boolean isSender) {
        return new Plan<>() {
            @Override
            public List<ActionActivator<BroadcastPeerAgent>> getInitialPlan() {
                ArrayList<ActionActivator<BroadcastPeerAgent>> myList = new ArrayList<>(List.of(
                        new ActionActivator<>(
                                ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                                new ACTakeRole<>(P2PenvName, RStochasticNetworkPeer.class, null)
                        ),
                        new ActionActivator<>(
                                ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                                new ACTakeRole<>(BenvName, RBroadcastPeer.class, null)
                        ),
                        new ActionActivator<>(
                                ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                                new AcAddCommunicationDelay<>(BenvName, null, "OUTPUT", "BASELINE", new UniformLevelFilter(1.0), new ContinuousUniformDelay(1, 20))
                        ),
                        new ActionActivator<>(
                                ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                                new AcSetBroadcastDeliveryHandler<>(BenvName, null, new TrivialTestBroadcastDeliveryHandler())
                        )
                ));
                if (isSender) {
                    myList.add(
                            new ActionActivator<>(
                                    ActivationScheduleFactory.ONE_SHOT_AT_ONE,
                                    new AcStartBroadcastOfMessage<>(BenvName, null, 1)
                            )
                    );
                }
                return myList;
            }
        };
    }

}
