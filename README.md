# max.model.broadcast.trivial_broadcast

This implements the interface for broadcasts from [max.model.broadcast.abstract_reliable_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.abstract_reliable_broadcast)
in the simplest manner possible.

It simply consists in broadcasting messages via the P2P layer 1 to N (sender included in receivers).

However this is NOT an implementation of a reliable broadcast algorithm.

It can be used if peculiarities of broadcast implementation can be abstracted away and if Byzantine behaviors 
(during the simulations) are such that they cannot impair the relevant properties of the reliable broadcast.

# Licence

__max.model.broadcast.trivial_broadcast__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













